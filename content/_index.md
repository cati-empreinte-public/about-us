
## EMPREINTE pour...

«Molecular PhEnotyping and bIochemical daTa Engineering»

### Contexte

Développement de nouvelles méthodes d’analyse à haut débit comme la métabolomique et la protéomique; 

Structuration d’une offre de services innovants au sein de plateformes technologiques et pilotage de la production de données omiques via les  infrastructures nationales (MetaboHUB, PROBE) et les réseaux métiers; 

Données générées de grande dimension et de nature différente, dont l’exploitation et l’interprétation nécessitent des compétences de pointe en analyses de données chimiques. 

### Missions

Constituer et animer une communauté pluridisciplinaire regroupant les acteurs INRAE impliqués dans l’utilisation des sciences du numérique pour mieux exploiter les données de la recherche; 

Mutualiser et capitaliser des expertises techniques sur la mise en oeuvre et/ou le développement de nouvelles méthodologies pour analyser, interpréter et intégrer ces données; 

Conduire des projets de recherche et développement transversaux aux unités du CATI pour améliorer l’exploitation des données de phénotypage moléculaire, implémenter le passage à une échelle plus large, et faciliter la mise en place de ressources numériques.

### Les communautés

Infrastructures scientifiques collectives de l’INRAE et son réseau distribué de plateformes technologiques de Métabolomique et de Protéomique ;

L'infrastructure de recherche nationale MetaboHUB et l'infrastructure de recherche INRAE PROBE ;

Les équipes de recherche des départements pilotes (ALIMH et TRANSFORM) et contributeurs (BAP) portant des questions de phénotypage multi-échelle ou faisant intervenir des problématique d'intégration de données


## EMPREINTE c'est...

### Métrique

28 agents en 2022 
3 départements de recherche INRAE (ALIMH, TRANSFORM et BAP) 
Budget de 8k€ annuel (hors APP) 
1 Assemblée générale annuelle 
2-3 animations scientifiques annuelles 
Porteur ou co organisateur de 2-3 événements annuels 
1 liste de diffusion interne - empreinte-interne@groupes.renater.fr 



### Trombi 2022 (suite à l'AG de Février)

![Trombi2022](page/220223_Trombi_Empreinte.png "Trombi2022")




